#include "../sln/queue/TProc.h"
#include <gtest.h>
TEST(TQueue, can_create_queue_with_positive_length)
{
	ASSERT_NO_THROW(TQueue q(25));
}
TEST(TQueue, throws_when_create_queue_with_negative_length)
{
	ASSERT_ANY_THROW(TQueue q(-1));
}
TEST(TQueue, queue_is_empty)
{
	TQueue q;
	q.Put(1);
	q.Get();
	EXPECT_TRUE(q.IsEmpty());
}
TEST(TQueue, queue_is_full)
{
	TQueue q(1);
	q.Put(1);
	EXPECT_TRUE(q.IsFull());
}
TEST(TQueue, queue_get_return_first_elem)
{
	TQueue q(3);
	q.Put(1);
	q.Put(2);
	q.Put(3);
	EXPECT_EQ(1, q.Get());
}
TEST(TQueue, queue_double_get_return_first_elem)
{
	TQueue q(4);
	q.Put(1);
	q.Put(2); 
	q.Put(3);
	q.Put(4);
	q.Get();
	EXPECT_EQ(2, q.Get());
}
TEST(TQueue, cant_get_from_empty_queue)
{
	TQueue q;
	q.Get();
	EXPECT_EQ(DataEmpty, q.GetRetCode());
}
TEST(TQueue, can_put_element_in_full_queue_and_return_valid_count)
{
	TQueue q(1);
	q.Put(1);
	q.Put(2);
	EXPECT_EQ(1, q.Get());
}
TEST(TQueue, can_get_ret_code_is_empty)
{
	TQueue q(1);
	q.Get();
	EXPECT_EQ(DataEmpty, q.GetRetCode());
}
TEST(TProc, can_create_proc_with_positive_Q)
{
	TQueue queue = TQueue();
	ASSERT_NO_THROW(TProc pr(1, &queue));
}
TEST(TProc, throws_when_create_proc_with_negative_Q)
{
	TQueue queue = TQueue();
	ASSERT_ANY_THROW(TProc pr(-1, &queue));
}